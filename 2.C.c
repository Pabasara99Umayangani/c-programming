// Write a C program that read a radius r and computes the area of a disk

#include <stdio.h>
int main()
{
   int disk_radius;
   float PI_VALUE=3.142, circle_area;

   printf("\nEnter radius of disk: ");
   scanf("%d",&disk_radius);

   circle_area = PI_VALUE * disk_radius * disk_radius;
   printf("\nArea of disk is: %f",circle_area);


   return 0;
}

